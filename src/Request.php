<?php
/**
 * HttpRequest lib
 *
 * @author Michiel van Baak <michiel@vanbaak.info>
 * @license MIT
 */
namespace mvanbaak\HttpRequest;

/**
 * Http request wrapper.
 * This class can make http requests using curl (if the module is installed and usable)
 * or with build in file_get_contents and a stream context
 *
 * This class does all the http requests to the bridge, and also
 * to the meethue site to discover the bridge ip address
 *
 * @author Michiel van Baak <michiel@vanbaak.info>
 * @license MIT
 */
class Request
{
    /** @var string The url to use in the object */
    private $url = '';
    /** @var string The request method */
    private $method = 'GET';
    /** @var array The data to send to the url */
    private $data = [];

    // Getters and Setters
    public function getUrl()
    {
        return $this->url;
    }
    public function setUrl($url)
    {
        $this->url = $url;
    }
    public function getMethod()
    {
        return $this->method;
    }
    public function setMethod($method)
    {
        if ($this->checkValidMethod($method)) {
            $this->method = $method;
        }
    }
    public function getData()
    {
        return $this->data;
    }
    public function setData($data)
    {
        if (is_array($data)) {
            $this->data = $data;
        } else {
            $this->data = array($data);
        }
    }
    // end Getters and Setters
    // helper functions for getters and setters
    /**
     * Check wether given method is a valid HTTP request method
     *
     * @todo implement logic
     * @param string $method The method name to check
     * @return boolean true if supported, false if not
     */
    private function checkValidMethod($method)
    {
        return true;
    }
    // end helper functions for getters and setters

    /**
     * Fire the actual HTTP request to the server
     * It finds out which type of library to use, either curl or builtin
     *
     * @return mixed Parsed api response from the server
     */
    private function doRequest()
    {
        // gather some info needed
        $url = $this->getUrl();
        $method = $this->getMethod();
        $data = $this->getData();

        // if url is not set, return error
        if (empty($url)) {
            return [
                'error' => [
                    'type' => 1404,
                    'address' => '',
                    'description' => 'No URL set',
                ],
            ];
        }

        // check if we have curl
        if (is_callable('curl_init')) {
            return $this->doCurlRequest($url, $method, $data);
        } else {
            return $this->doNativeRequest($url, $method, $data);
        }
    }

    /**
     * Do the HTTP request with CURL
     *
     * @param string $url The URL endpoint to call
     * @param string $method The HTTP Request Method (eg: POST, GET, PUT)
     * @param mixed $data The body of the HTTP Request
     * @return mixed parsed json from the URL
     */
    private function doCurlRequest($url, $method, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        switch($method) {
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, true);
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_PUT, true);
                break;
            case 'GET':
            default:
                curl_setopt($ch, CURLOPT_HTTPGET, true);
                break;
        }
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        $result = curl_exec($ch);
        if ($result === false) {
            return '';
        }
        return $result;
    }

    /**
     * Do the HTTP request with native stream_* things
     *
     * @param string $url The URL endpoint to call
     * @param string $method The HTTP Request Method (eg: POST, GET, PUT)
     * @param mixed $data The body of the HTTP Request
     * @return mixed parsed json from the URL
     */
    private function doNativeRequest($url, $method, $data)
    {
        // build options for creating a stream context
        $options = [
            'http' => [
                'method' => $method,
                'header' => "Content-Type: application/json\r\n"
            ],
        ];
        // if data is set, add it to the stream context options
        if (!empty($data)) {
            $options['http']['content'] = json_encode($data);
        }

        // create context
        $context = stream_context_create($options);

        // get data from api server
        $result = file_get_contents($url, false, $context);
        if ($result === false) {
            return '';
        }
        return $result;
    }

    /**
     * Do a OPTIONS request
     *
     * @param string $url The URL to OPTION
     * @param mixed $data The body of the POST
     * @return string the return from the URL
     */
    public function doOPTIONS($url, $data = '')
    {
        $this->setUrl($url);
        $this->setMethod('OPTIONS');
        $this->setData($data);
        return $this->doRequest();
    }

    /**
     * Do a GET request
     *
     * @param string $url The URL to GET
     * @return string the return from the URL
     */
    public function doGet($url)
    {
        $this->setUrl($url);
        $this->setMethod('GET');
        return $this->doRequest();
    }

    /**
     * Do a HEAD request
     *
     * @param string $url The URL to HEAD
     * @param mixed $data The body of the HEAD
     * @return string empty string as a HEAD should never return something
     */
    public function doHEAD($url, $data = '')
    {
        $this->setUrl($url);
        $this->setMethod('HEAD');
        $this->setData($data);
        return $this->doRequest();
    }

    /**
     * Do a POST request
     *
     * @param string $url The URL to POST to
     * @param mixed $data The body of the POST
     * @return string the return from the URL
     */
    public function doPost($url, $data)
    {
        $this->setUrl($url);
        $this->setMethod('POST');
        $this->setData($data);
        return $this->doRequest();
    }

    /**
     * Do a PUT request
     *
     * @param string $url The URL to PUT to
     * @param mixed data The body of the PUT
     * @return string the return from the URL
     */
    public function doPut($url, $data = '')
    {
        $this->setUrl($url);
        $this->setMethod('PUT');
        $this->setData($data);
        return $this->doRequest();
    }

    /**
     * Do a DELETE request
     *
     * @param string $url The URL to DELETE to
     * @return string the return from the URL
     */
    public function doDelete($url)
    {
        $this->setUrl($url);
        $this->setMethod('DELETE');
        $this->setData($data);
        return $this->doRequest();
    }
}

/* vim: set expandtab ts=4 sw=4 ai */
