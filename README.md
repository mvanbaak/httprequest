# mvanbaak/httprequest
[![Build Status](https://drone.io/bitbucket.org/mvanbaak/httprequest/status.png)](https://drone.io/bitbucket.org/mvanbaak/httprequest/latest)
[![Latest Stable Version](https://poser.pugx.org/mvanbaak/httprequest/v/stable.svg)](https://packagist.org/packages/mvanbaak/httprequest)
[![Total Downloads](https://poser.pugx.org/mvanbaak/httprequest/downloads.svg)](https://packagist.org/packages/mvanbaak/httprequest)
[![Latest Unstable Version](https://poser.pugx.org/mvanbaak/httprequest/v/unstable.svg)](https://packagist.org/packages/mvanbaak/httprequest)
[![License](https://poser.pugx.org/mvanbaak/httprequest/license.svg)](https://packagist.org/packages/mvanbaak/httprequest)
## Requirements

Requires PHP 5.4.0 (or later).

## Installation
To add the library as a local, per-project dependency use [Composer](http://getcomposer.org)!
```json
{
	"require": {
		"mvanbaak/httprequest": "dev-master"
	}
}
```

## Contributing
If you would like to contribute, please use the build process for any changes
and after the build passes, send a pull request on bitbucket!
```sh
./build.php
```
